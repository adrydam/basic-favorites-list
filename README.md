## How to run the project?

Execute the following commands:

    docker compose build --parallel && docker compose up -d

After process is completed, install the dependencies in the **PHP container:**

    docker exec -it php bash
    composer install
    php bin/console doctrine:migrations:migrate
    php bin/console doctrine:fixtures:load

Later, install the JS dependencies in the **Node containter:**

    docker exec -it node bash
    npm install && yarn install
    yarn encore dev

Then, you should be able to load the website:

    http://localhost:8001

