var Encore = require('@symfony/webpack-encore');

Encore
	.setOutputPath('public/build/')
	.setPublicPath('/build')
	.cleanupOutputBeforeBuild()
	.enableSourceMaps(!Encore.isProduction())
	.addEntry('app', './assets/js/app.js')
	.enableSingleRuntimeChunk()
   // .addStyleEntry('css/app', './assets/css/app.scss')
   // .enableSassLoader()
   // Enable Vue loader
	.enableVueLoader()
;
module.exports = Encore.getWebpackConfig();
