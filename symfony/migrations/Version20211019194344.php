<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211019194344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE articles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, datetime DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favorites (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favorites_users (favorites_id INT NOT NULL, users_id INT NOT NULL, INDEX IDX_AA13766784DDC6B4 (favorites_id), INDEX IDX_AA13766767B3B43D (users_id), PRIMARY KEY(favorites_id, users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favorites_articles (favorites_id INT NOT NULL, articles_id INT NOT NULL, INDEX IDX_E6C02C484DDC6B4 (favorites_id), INDEX IDX_E6C02C41EBAF6CC (articles_id), PRIMARY KEY(favorites_id, articles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE favorites_users ADD CONSTRAINT FK_AA13766784DDC6B4 FOREIGN KEY (favorites_id) REFERENCES favorites (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favorites_users ADD CONSTRAINT FK_AA13766767B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favorites_articles ADD CONSTRAINT FK_E6C02C484DDC6B4 FOREIGN KEY (favorites_id) REFERENCES favorites (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favorites_articles ADD CONSTRAINT FK_E6C02C41EBAF6CC FOREIGN KEY (articles_id) REFERENCES articles (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE favorites_articles DROP FOREIGN KEY FK_E6C02C41EBAF6CC');
        $this->addSql('ALTER TABLE favorites_users DROP FOREIGN KEY FK_AA13766784DDC6B4');
        $this->addSql('ALTER TABLE favorites_articles DROP FOREIGN KEY FK_E6C02C484DDC6B4');
        $this->addSql('ALTER TABLE favorites_users DROP FOREIGN KEY FK_AA13766767B3B43D');
        $this->addSql('DROP TABLE articles');
        $this->addSql('DROP TABLE favorites');
        $this->addSql('DROP TABLE favorites_users');
        $this->addSql('DROP TABLE favorites_articles');
        $this->addSql('DROP TABLE users');
    }
}
