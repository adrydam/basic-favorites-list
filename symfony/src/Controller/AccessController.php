<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/access')]
class AccessController extends AbstractController
{
    public $badrequestStatus = 400;
    public $unauthorizedStatus = 401;
    public $notFoundStatus = 404;
    private $usersRepository;
    private $entityManager;

    public function __construct(
        UsersRepository $usersRepository, 
        EntityManagerInterface $entityManager
    ) {
        $this->usersRepository = $usersRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/login', name: 'access_login', methods:['POST'])]
    public function login(Request $request): JsonResponse
    {
        try {
            $input = json_decode($request->getContent(), true);
            $user = $this->usersRepository->findOneByUsername($input['username']);

            if ($user->getPassword() === $input['password']) {
                $session = $request->getSession();
                $session->set('user', $user);
            }

            return new JsonResponse([ 'success' => true ]);
        } catch (\Throwable $th) {
            return new JsonResponse([ 
                'success' => false, 
                'msg' => 'Error login - please check your credentials', 
            ], $this->unauthorizedStatus);
        }
    }

    #[Route('/signup', name: 'access_signup', methods:['POST'])]
    public function register(Request $request): JsonResponse
    {
        try {
            $input = json_decode($request->getContent(), true);

            $createUser = new Users();
            $createUser->setUsername($input['username']);
            $createUser->setPassword($input['password']);
            $this->entityManager->persist($createUser);
            $this->entityManager->flush();

            $session = $request->getSession();
            $session->set('user', $createUser);

            return new JsonResponse([ 'success' => true ]);
        } catch (\Throwable $th) {
            return new JsonResponse([ 'success' => false, 'msg' => 'Error sign up - The username is already registered' ], $this->badrequestStatus);
        }
    }

    #[Route('/logout', name: 'access_logout', methods:['GET'])]
    public function logout(Request $request): JsonResponse
    {
        try {
            $user = $request->getSession()->remove('user');

            return new JsonResponse([ 
                'success' => true,
                'isLogged' => $request->getSession()->get('user') ?? false
            ]);
        } catch (\Throwable $th) {
            return new JsonResponse([ 'success' => false, 'msg' => 'Error' ], $this->badrequestStatus);
        }
    }

    #[Route('/check', name: 'access_check', methods:['GET'])]
    public function verifyLogged(Request $request): JsonResponse
    {
        try {
            $user = $request->getSession()->get('user');

            return new JsonResponse([ 
                'success' => true,
                'isLogged' => $user instanceof Users,
                'favorites' => (array)$user->getFavorites()
            ]);
        } catch (\Throwable $th) {
            return new JsonResponse([ 'success' => false, 'msg' => 'Please, log in again' ], $this->unauthorizedStatus);
        }
    }
}
