<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/favorites')]
class FavoritesController extends AbstractController
{
    public $badrequestStatus = 400;
    public $unauthorizedStatus = 401;
    public $notFoundStatus = 404;
    private $articlesRepository;
    private $entityManager;

    public function __construct(
        ArticlesRepository $articlesRepository, 
        EntityManagerInterface $entityManager
    ) {
        $this->articlesRepository = $articlesRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/add/{id}', name: 'favorites_add', methods:['GET'])]
    public function addFavorite(int $id, Request $request): JsonResponse
    {
        try {
            $user = $request->getSession()->get('user');
            $article = $this->articlesRepository->find($id);

            $user->addFavorites($article);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return new JsonResponse([ 
                'success' => true, 
                'msg' => 'Favorite added',
                'favorites' => $user->getFavorites()
            ]);

        } catch (\Throwable $th) {
            return new JsonResponse([ 
                'success' => false, 
                'msg' => 'Error adding to favorites',
                'trace' => $th->getMessage()
            ], $this->badrequestStatus);
        }
    }
}
