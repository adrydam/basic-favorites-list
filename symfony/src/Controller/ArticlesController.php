<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/articles')]
class ArticlesController extends AbstractController
{
    private $articlesRepository;

    public function __construct(ArticlesRepository $articlesRepository) {
        $this->articlesRepository = $articlesRepository;
    }

    #[Route('/get', name: 'get_articles', methods:['GET'])]
    public function getArticlesList(Request $request): JsonResponse
    {
        $articles = $this->articlesRepository->getArticlesList();
        return new JsonResponse($articles);
    }
}
