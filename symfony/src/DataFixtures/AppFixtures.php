<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Articles;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($item = 0; $item < 101; $item++) {
            $article = new Articles();
            $article->setName('Item Name '.$item);
            $article->setPrice(rand(1, 1000));
            $article->setDatetime(new DateTime('NOW'));

            $manager->persist($article);
        }

        $manager->flush();
    }
}
